This project was started to test my web scraping, encoding, and SSG skills.
Almost all content is from the TemTem Official Wiki, but processed down from
about 2 GB to around 57 MB.

I started by downloading icons, idle animations, and cries pertinent to all 
original TemTem species.  Compression was mostly done using `ffmpeg`, but
species icons were edited using Krita to increase page load speed. (TemTem Wiki
overlays a background and foreground over their species icons, but I found
things load much faster if everything was flattened into one WEBP file)

Text information was scrapped into JSON format, `temtem.json`, using JavaScript 
that loaded every species page and then used `querySelector` to gather pertinent
info.  I then used a custom Python script, `build.py`, to generate the site.
After that, all that was left was to write some simple CSS.
