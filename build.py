import json, os

temtems = json.load(open('temtem.json'))

# This build process needs to be refined for installing an app in a subdirectory of the host
# i.e. example.com/subdir/

for temtem in temtems:
  typeItems = '<img alt="{type}" title="{type}" height="45" width="45" src="{path}temtem.wiki.gg/icons/{type}.webp">'.format(type=temtem['type'][0], path='{path}')
  
  for extraType in temtem['type'][1:]:
    typeItems += '<img alt="{type}" title="{type}" height="45" width="45" src="{path}temtem.wiki.gg/icons/{type}.webp">'.format(type=extraType, path='{path}')
    
  template = '''<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8"/>
    <title>{name}</title>
    <link rel="stylesheet" href="{path}temtem.css">
    <meta name="description" content="{tempedia}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{path}temtem.wiki.gg/icons/{name}.webp">
    <link rel="manifest" href="{path}manifest.json">
    <meta name="theme-color" content="#1a1a1a">
    <script type="module" src="{path}app.js"></script>
    <script type="module" src="{path}luma.js"></script>
  </head>
  <body>
    <h1>{name}</h1>
    <figure>
      <figcaption>#{no}</figcaption>
      <audio autoplay src="{path}temtem.wiki.gg/cry/{name}.opus"></audio>
      <video alt="TemTem" width="320" height="320" autoplay loop src="{path}temtem.wiki.gg/idle animations/{variantDir}{name}.webm"></video>
    </figure>
    <p>{tempedia}</p>
    <dl>
      <dt>Type</dt>
      <dd>
        {typeItems}
      </dd>
      <dt>Height</dt>
      <dd>{height} cm</dd>
      <dt>Weight</dt>
      <dd>{weight} kg</dd>
    </dl>
    <nav>
      <a class="temtem" href="{path}{prev}.html" rel="prev"><img alt="{prev}" width="80" height="80" src="{path}temtem.wiki.gg/icons/{prev}.webp"></a>
      <a class="temtem" href="{path}{next}.html" rel="next"><img alt="{next}" width="80" height="80" src="{path}temtem.wiki.gg/icons/{next}.webp"></a>
    </nav>
    <footer>
      <a rel="noreferrer" href="https://temtem.wiki.gg/{name}"><img src="{path}temtem.wiki.gg/icons/Site-logo.webp" height="128" width="233" alt="TemTem Wiki"></a>
    </footer>
  </body>
</html>'''.format(typeItems=typeItems, name='{name}', tempedia='{tempedia}', no='{no}', height='{height}', weight='{weight}', prev='{prev}', next='{next}', variant='{variant}', path='{path}', variantDir='{variantDir}')
    
  temFile = open('{}.html'.format(temtem['name']),'w+')
  temFile.write(template.format(path='./', name=temtem['name'], tempedia=temtem['tempedia'], no=temtem['no'], height=temtem['height'], weight=temtem['weight'], prev=temtem['prev'], next=temtem['next'], variant='', variantDir=''))
  temFile.close()

  lumaFile = open('Luma{}.html'.format(temtem['name']), 'w+')
  lumaFile.write(template.format(path='./', name=temtem['name'], tempedia=temtem['tempedia'], no=temtem['no'], height=temtem['height'], weight=temtem['weight'], prev=temtem['prev'], next=temtem['next'], variant='Luma', variantDir='luma/'))
  lumaFile.close()
