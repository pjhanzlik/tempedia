// luma appear at a 1 in 7500 chance in the wild
// two luma parents have a 1 in 100 chance of producing a luma child
// to make it more likely visitors see lumas, lets use the breeding number


// Replace temtem links with the luma variant randomly
if (Math.random() < 1 / 100) {
  const temtems = document.querySelectorAll('a.temtem');
  for(const temtem of temtems) {
    const [_, path, filename] = /(.+\/)(\w+.html)/.exec(temtem.href);
    temtem.href = `${path}Luma${filename}`
  }

}
