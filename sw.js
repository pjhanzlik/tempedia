// To simplify design I went with a 'install everything' approach
// I hope ~215.1MB per visitor is okay with my host
// Should look into size savings using glTF with posters for temtem renders
self.addEventListener("install", (event)=>{
  const cacheGoogleFonts = async()=>{
    const googleFonts = await caches.open("fonts.google.com");
    await googleFonts.add('./fonts.google.com/MavenPro-VariableFont_wght.ttf');
  }

  const cacheRoot = async()=>{
    const root = await caches.open('root');

    const rootFiles = ['', 'index.html', 'app.js', 'luma.js', 'manifest.json', 'tempedia.css', 'temtem.css'].map((file)=>`./${file}`)
    await root.addAll(rootFiles)
  }

  const cacheTemTem = async()=>{
    const temtem = ["0b10", "Gharunder", "Minttle", "Shaolite", "0b1", "Golzy", "Mitty", "Sherald", "Adoroboros", "Goolder", "Molgu", "Shuine", "Akranox", "Gorong", "Momo", "Skail", "Amphatyr", "Goty", "Monkko", "Skunch", "Ampling", "Granpah", "Mosu", "Smazee", "Anahir", "Grumper", "Mouflank", "Smolzy", "Anatan", "Grumvel", "Mudrid", "Sparzy", "Aohi", "Gyalis", "Mushi", "Spriole", "Arachnyte", "Halzhi", "Mushook", "Swali", "Azuroc", "Hazrat", "Myx", "Taifu", "Babawa", "Hedgine", "Nagaise", "Tateru", "Baboong", "Hidody", "Nessla", "Tental", "Banapi", "Hocus", "Nidrasil", "Thaiko", "Barnshe", "Hoglip", "Noxolotl", "Tortenite", "Bigu", "Houchic", "Occlura", "Towly", "Blooze", "Innki", "Oceara", "Toxolotl", "Broccoblin", "Kaku", "Oree", "Tukai", "Broccolem", "Kalabyss", "Orphyll", "Tulcan", "Broccorc", "Kalazu", "Osuchi", "Turoc", "Bunbun", "Kauren", "Osukai", "Tutsu", "Capyre", "Kinu", "Osukan", "Tuvine", "Cerneaf", "Owlhe", "Tuwai", "Chimurian", "Paharac", "Tuwire", "Paharo", "Tyranak", "Pewki", "Ukama", "Pigepic", "Umishi", "Piraniant", "Valash", "Platimous", "Valiar", "Platox", "Venmet", "Platypet", "Vental", "Pocus", "Venx", "Koish", "Pupoise", "Volarend", "Pycko", "Volgon", "Chromeon", "Kuri", "Raiber", "Vulcrane", "Lapinite", "Raican", "Vulffy", "Chubee", "Loali", "Raignet", "Vulor", "Crystle", "Loatle", "Raize", "Vulvir", "Cycrox", "Magmis", "Reval", "Waspeen", "Deendre", "Magmut", "Rhoulder", "Wiplump", "Drakash", "Maoala", "Saipat", "Yowlar", "Droply", "Mastione", "Saku", "Zaobian", "Fomu", "Mawmense", "Sanbi", "Zenoreth", "Galvanid", "Mawtle", "Scaravolt", "Zephyruff", "Ganki", "Mimit", "Scarawatt", "Zizare", "Garyo", "Minothor", "Seismunch", "Gazuma", "Minox", "Shaolant"]

    const cache = await caches.open('temtem.wiki.gg');

    const resources = [
      // regular HTML pages
      ...temtem.map((tem)=>`./${tem}.html`),
      // regular icons
      ...temtem.concat(['Icon', 'Neutral', 'Wind', 'Earth', 'Water', 'Fire', 'Nature', 'Electric', 'Mental', 'Digital', 'Melee', 'Crystal', 'Toxic', 'Site-logo', 'small icon', 'Big Icon']).map((tem)=>`./temtem.wiki.gg/icons/${tem}.webp`),
      // luma html pages
      ...temtem.map((tem)=>`./Luma${tem}.html`),
      // maskable icons
      ...[192,512].map((size)=>`./temtem.wiki.gg/icons/maskable_icon_x${size}.png`),
      // regular idle animations
      ...temtem.map((tem)=>`./temtem.wiki.gg/idle animations/${tem}.webm`),
      // cries
      ...temtem.map((tem)=>`./temtem.wiki.gg/cry/${tem}.opus`),
      // luma idle animations
      ...temtem.map((tem)=>`./temtem.wiki.gg/idle animations/luma/${tem}.webm`)
    ]

    await cache.addAll(resources)
  }

  event.waitUntil(Promise.all([cacheGoogleFonts(), cacheRoot(), cacheTemTem()]))
}
);

self.addEventListener("fetch", (event)=>{
  // Stale-While-Revalidate might be a better strategy for production
  // but we are hosting this on free servers and don't want to bug them
  const cacheFirst = async (request) => {
    const responseFromCache = await caches.match(request);
    if (responseFromCache) {
      return responseFromCache;
    }
    return fetch(request);
  };
  
  event.respondWith(cacheFirst(event.request));
}
);
